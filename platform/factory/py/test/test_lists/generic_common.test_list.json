{
  "inherit": [
    "base.test_list"
  ],
  "constants": {
    "default_factory_server_url": "",
    "__comment_default_factory_server_url": "The default URL to factory server, if not defined in locals.",
    "enable_factory_server": true,
    "__comment_enable_factory_server": "Enable factory_server. Change this to false by adding 'disable_factory_server.test_list' to beginning of inherit list in main test list. Note that some factory environment might not need a shopfloor.",
    "hwid_need_vpd": false,
    "__comment_hwid_need_vpd": "Set to true if some rules in the HWID database rely on VPD data",
    "is_chromebox": false,
    "rma_mode": false,
    "sd": {
      "sysfs_path": "/sys/devices/pci0000:00/0000:00:xx.y/mmc_host"
    },
    "__comment_sd_sysfs_path": "Use 'udevadm monitor' to find out udev sysfs path.",
    "typec_usb": {
      "left": {
        "usb2_sysfs_path": "/sys/devices/pci0000:00/0000:00:xx.y/usb1/?-?",
        "usb3_sysfs_path": "/sys/devices/pci0000:00/0000:00:xx.y/usb2/?-?",
        "usbpd_id": -1,
        "display_info": [
          "DisplayPort",
          "DP-1"
        ]
      },
      "right": {
        "usb2_sysfs_path": "/sys/devices/pci0000:00/0000:00:xx.y/usb1/?-?",
        "usb3_sysfs_path": "/sys/devices/pci0000:00/0000:00:xx.y/usb2/?-?",
        "usbpd_id": -1,
        "display_info": [
          "DisplayPort",
          "DP-2"
        ]
      }
    }
  },
  "definitions": {
    "AccelerometersCalibration": {
      "pytest_name": "accelerometers_calibration",
      "args": {
        "orientation": {
          "in_accel_x": 0,
          "in_accel_y": 0,
          "in_accel_z": 1
        },
        "spec_offset": [
          0.5,
          0.5
        ]
      }
    },
    "AccelerometersLidAngle": {
      "pytest_name": "accelerometers_lid_angle",
      "args": {
        "spec_offset": [
          0.5,
          0.5
        ]
      }
    },
    "ActivateRegCode": {
      "inherit": "ShopfloorService",
      "label": "Activate Registration Code",
      "args": {
        "method": "ActivateRegCode"
      }
    },
    "AllCheckPoint": {
      "inherit": "CheckPoint",
      "label": "Check Point for All Tests",
      "args": {
        "include_parents": true
      }
    },
    "AudioJack": {
      "pytest_name": "audio_loop",
      "label": "Audio Jack",
      "args": {
        "require_dongle": true,
        "check_dongle": true,
        "tests_to_conduct": [
          {
            "type": "sinewav",
            "freq_threshold": 300,
            "rms_threshold": [
              0.08,
              null
            ]
          }
        ]
      }
    },
    "Backlight": {
      "pytest_name": "backlight"
    },
    "BadBlocks": {
      "pytest_name": "bad_blocks",
      "__comment": "When run alone, this takes ~.5s/MiB (for four passes).  We'll do a gigabyte, which takes about 9 minutes.",
      "args": {
        "timeout_secs": 120,
        "log_threshold_secs": 10,
        "max_bytes": 1073741824
      }
    },
    "Barrier": {
      "pytest_name": "summary",
      "label": "Barrier",
      "allow_reboot": true,
      "action_on_failure": "STOP",
      "disable_abort": true,
      "args": {
        "disable_input_on_fail": true,
        "pass_without_prompt": true,
        "accessibility": true
      }
    },
    "BaseAccelerometersCalibration": {
      "inherit": "AccelerometersCalibration",
      "args": {
        "location": "base"
      }
    },
    "Battery": {
      "pytest_name": "battery"
    },
    "BatterySysfs": {
      "pytest_name": "battery_sysfs"
    },
    "BlockingCharge": {
      "pytest_name": "blocking_charge",
      "exclusive_resources": [
        "POWER"
      ]
    },
    "Bluetooth": {
      "pytest_name": "bluetooth",
      "args": {
        "expected_adapter_count": 1,
        "scan_devices": true,
        "average_rssi_threshold": -55.0
      }
    },
    "Button": {
      "pytest_name": "button",
      "args": {
        "timeout_secs": 120
      }
    },
    "Camera": {
      "pytest_name": "camera"
    },
    "CameraManual": {
      "inherit": "Camera",
      "args": {
        "mode": "manual"
      }
    },
    "ChargeDischargeCurrent": {
      "pytest_name": "battery_current",
      "label": "Charge Discharge Current",
      "exclusive_resources": [
        "POWER"
      ],
      "args": {
        "min_charging_current": 150,
        "min_discharging_current": 400,
        "timeout_secs": 30,
        "max_battery_level": 90
      }
    },
    "ChargerTypeDetection": {
      "pytest_name": "ac_power",
      "label": "Charger Type Detection"
    },
    "CheckCr50BoardIDNotSet": {
      "pytest_name": "check_cr50_board_id",
      "label": "Check Cr50 Board ID Not Set",
      "args": {
        "board_id_type": "ffffffff",
        "board_id_flags": "ffffffff"
      }
    },
    "CheckPoint": {
      "inherit": "Barrier",
      "label": "Check Point",
      "args": {
        "pass_without_prompt": false
      }
    },
    "CheckReleaseImage": {
      "pytest_name": "check_image_version",
      "label": "Check Release Image",
      "args": {
        "check_release_image": true,
        "use_netboot": false
      }
    },
    "CheckSerialNumber": {
      "pytest_name": "check_serial_number",
      "label": "Check Serial Number"
    },
    "ClearFactoryVPDEntries": {
      "inherit": "TestGroup",
      "label": "Clear factory VPD entries",
      "subtests": [
        {
          "inherit": "ExecShell",
          "label": "Clear factory VPD entries",
          "args": {
            "commands": "gooftool clear_factory_vpd_entries"
          }
        },
        "RebootStep"
      ]
    },
    "ClearTPMOwnerRequest": {
      "pytest_name": "tpm_clear_owner"
    },
    "ColdReset": {
      "inherit": "TestGroup",
      "label": "Cold Reset",
      "subtests": [
        {
          "inherit": "ExecShell",
          "label": "EC Cold Reset",
          "args": {
            "commands": "ectool reboot_ec cold at-shutdown"
          }
        },
        "HaltStep"
      ]
    },
    "Display": {
      "pytest_name": "display"
    },
    "DisplayPoint": {
      "pytest_name": "display_point",
      "args": {
        "max_point_count": 5
      }
    },
    "ExecShell": {
      "pytest_name": "exec_shell"
    },
    "ExternalDisplay": {
      "pytest_name": "external_display",
      "args": {
        "main_display": "eDP-1",
        "display_info": [
          "External Dispaly",
          "HDMI",
          null,
          0
        ]
      }
    },
    "Finish": {
      "inherit": "Message",
      "label": "Finish"
    },
    "FlashNetboot": {
      "pytest_name": "flash_netboot"
    },
    "FlushTestlog": {
      "inherit": "SyncFactoryServer",
      "args": {
        "sync_event_logs": false,
        "update_toolkit": false,
        "upload_report": false,
        "upload_reg_codes": false,
        "flush_testlog": true
      }
    },
    "GetDeviceInfo": {
      "inherit": "ShopfloorService",
      "args": {
        "method": "GetDeviceInfo"
      }
    },
    "Gyroscope": {
      "pytest_name": "gyroscope",
      "label": "Gyroscope",
      "args": {
        "rotation_threshold": 1.0,
        "stop_threshold": 0.1
      }
    },
    "GyroscopeCalibration": {
      "pytest_name": "gyroscope_calibration",
      "label": "Gyroscope Calibration"
    },
    "HWButton": {
      "inherit": "TestGroup",
      "label": "Hardware Button",
      "subtests": [
        {
          "inherit": "Button",
          "label": "Volume Down",
          "args": {
            "button_key_name": "KEY_VOLUMEDOWN",
            "button_name": "i18n! Volume Down"
          }
        },
        {
          "inherit": "Button",
          "label": "Volume Up",
          "args": {
            "button_key_name": "KEY_VOLUMEUP",
            "button_name": "i18n! Volume Up"
          }
        },
        {
          "inherit": "Button",
          "label": "Power Button",
          "args": {
            "button_key_name": "KEY_POWER",
            "button_name": "i18n! Power Button"
          }
        }
      ]
    },
    "Idle": {
      "pytest_name": "nop",
      "label": "Idle"
    },
    "Keyboard": {
      "pytest_name": "keyboard",
      "args": {
        "allow_multi_keys": true
      }
    },
    "KeyboardBacklight": {
      "pytest_name": "keyboard_backlight"
    },
    "LED": {
      "pytest_name": "led",
      "args": {
        "challenge": true
      }
    },
    "LidAccelerometersCalibration": {
      "inherit": "AccelerometersCalibration",
      "args": {
        "location": "lid"
      }
    },
    "LidSwitch": {
      "pytest_name": "lid_switch"
    },
    "MRCCache": {
      "label": "MRC Cache",
      "subtests": [
        {
          "pytest_name": "mrc_cache",
          "label": "Create Cache",
          "args": {
            "mode": "create"
          }
        },
        "RebootStep",
        {
          "pytest_name": "mrc_cache",
          "label": "Verify Cache",
          "args": {
            "mode": "verify"
          }
        }
      ]
    },
    "Message": {
      "pytest_name": "message",
      "allow_reboot": true
    },
    "ModelSKU": {
      "pytest_name": "model_sku",
      "label": "Model and SKU",
      "action_on_failure": "STOP"
    },
    "ModemSecurity": {
      "pytest_name": "modem_security",
      "disable_services": [
        "modemmanager"
      ]
    },
    "PartitionTable": {
      "pytest_name": "partition_table"
    },
    "Placeholder": {
      "pytest_name": "nop",
      "label": "Placeholder"
    },
    "Probe": {
      "pytest_name": "probe.probe",
      "label": "Probe Hardware",
      "args": {
        "config_file": "probe.json"
      }
    },
    "QRScan": {
      "inherit": "Camera",
      "label": "QR Scan",
      "args": {
        "mode": "qr",
        "QR_string": "Hello ChromeOS!"
      }
    },
    "ReSignReleaseKernel": {
      "pytest_name": "update_kernel",
      "run_if": "constants.phase != 'PVT' and constants.grt.re_sign_release_kernel",
      "args": {
        "to_release": true
      }
    },
    "ReadDeviceDataFromVPD": {
      "pytest_name": "read_device_data_from_vpd"
    },
    "RemovableStorage": {
      "pytest_name": "removable_storage",
      "args": {
        "block_size": 524288,
        "perform_random_test": false,
        "perform_sequential_test": true,
        "sequential_block_count": 8
      }
    },
    "SDPerformance": {
      "inherit": "RemovableStorage",
      "label": "SD Performance",
      "args": {
        "media": "SD",
        "sysfs_path": "eval! constants.sd.sysfs_path"
      }
    },
    "Scan": {
      "pytest_name": "scan"
    },
    "ShopfloorNotifyEnd": {
      "inherit": "ShopfloorService",
      "label": "Shopfloor Service (NotifyEnd)",
      "args": {
        "method": "NotifyEnd",
        "args": [
          "eval! locals.station"
        ]
      }
    },
    "ShopfloorNotifyStart": {
      "inherit": "ShopfloorService",
      "label": "Shopfloor Service (NotifyStart)",
      "args": {
        "method": "NotifyStart",
        "args": [
          "eval! locals.station"
        ]
      }
    },
    "ShopfloorService": {
      "pytest_name": "shopfloor_service",
      "run_if": "constants.enable_factory_server"
    },
    "SpatialSensorCalibration": {
      "pytest_name": "spatial_sensor_calibration"
    },
    "SpeakerDMic": {
      "pytest_name": "audio_loop",
      "label": "Speaker/Microphone",
      "args": {
        "require_dongle": false,
        "check_dongle": true,
        "tests_to_conduct": [
          {
            "type": "audiofun",
            "duration": 4,
            "threshold": 80,
            "volume_gain": 50
          }
        ]
      }
    },
    "Start": {
      "pytest_name": "start",
      "allow_reboot": true
    },
    "StationEnd": {
      "inherit": "FlattenGroup",
      "subtests": [
        "StationEndSyncFactoryServer",
        "ShopfloorNotifyEnd",
        "Barrier",
        "WriteDeviceDataToVPD"
      ]
    },
    "StationEndSyncFactoryServer": {
      "inherit": "SyncFactoryServerUploadReport",
      "args": {
        "upload_report": "eval! locals.station_end_upload_report"
      }
    },
    "StationStart": {
      "inherit": "FlattenGroup",
      "subtests": [
        "SyncFactoryServer",
        "ShopfloorNotifyStart"
      ]
    },
    "StressAppTest": {
      "pytest_name": "stressapptest",
      "label": "Stress App Test",
      "exclusive_resources": [
        "CPU"
      ]
    },
    "Stylus": {
      "pytest_name": "stylus"
    },
    "SuspendResume": {
      "pytest_name": "suspend_resume"
    },
    "SyncFactoryServer": {
      "pytest_name": "sync_factory_server",
      "run_if": "constants.enable_factory_server",
      "args": {
        "server_url": "eval! constants.default_factory_server_url"
      }
    },
    "SyncFactoryServerUploadReport": {
      "inherit": "SyncFactoryServer",
      "args": {
        "upload_report": true,
        "report_stage": "eval! locals.station"
      }
    },
    "TPMVerifyEK": {
      "pytest_name": "tpm_verify_ek"
    },
    "TabletMode": {
      "pytest_name": "tablet_mode"
    },
    "TabletRotation": {
      "pytest_name": "tablet_rotation"
    },
    "ThermalSensors": {
      "pytest_name": "thermal_sensors"
    },
    "Touchpad": {
      "pytest_name": "touchpad"
    },
    "Touchscreen": {
      "pytest_name": "touchscreen"
    },
    "TouchscreenUniformity": {
      "pytest_name": "touch_uniformity",
      "label": "Touchscreen Uniformity"
    },
    "URandom": {
      "pytest_name": "urandom",
      "label": "Random Number Generation"
    },
    "USBPerformance": {
      "inherit": "RemovableStorage",
      "label": "USB Performance",
      "args": {
        "media": "USB"
      }
    },
    "USBTypeCManualBase": {
      "inherit": "TestGroup",
      "subtests": [
        {
          "inherit": "USBPerformance",
          "label": "USB3 CC1 Performance",
          "args": {
            "sysfs_path": "eval! locals.usb.usb3_sysfs_path",
            "usbpd_port_polarity": [
              "eval! locals.usb.usbpd_id",
              1
            ]
          }
        },
        {
          "inherit": "USBPerformance",
          "label": "USB3 CC2 Performance",
          "args": {
            "sysfs_path": "eval! locals.usb.usb3_sysfs_path",
            "usbpd_port_polarity": [
              "eval! locals.usb.usbpd_id",
              2
            ]
          }
        },
        {
          "inherit": "USBPerformance",
          "label": "USB2 Performance",
          "args": {
            "sysfs_path": "eval! locals.usb.usb2_sysfs_path"
          }
        },
        "USBTypeCManualChargeItems",
        {
          "inherit": "ExternalDisplay",
          "label": "USB3 External Display Test",
          "args": {
            "display_info": [
              "eval! locals.usb.display_info"
            ]
          }
        },
        "Barrier"
      ]
    },
    "USBTypeCManualCharge": {
      "inherit": "ChargeDischargeCurrent",
      "args": {
        "usbpd_info": [
          "eval! locals.usb.usbpd_id",
          "eval! locals.voltage * 1000 - 500",
          "eval! locals.voltage * 1000 + 500"
        ],
        "usbpd_prompt": "eval! locals.usb_label"
      }
    },
    "USBTypeCManualChargeItems": {
      "inherit": "FlattenGroup",
      "subtests": [
        {
          "inherit": "USBTypeCManualCharge",
          "label": "20V Charging",
          "locals": {
            "voltage": 20
          }
        },
        {
          "inherit": "USBTypeCManualCharge",
          "label": "5V Charging",
          "locals": {
            "voltage": 5
          }
        }
      ]
    },
    "USBTypeCManualLeft": {
      "inherit": "USBTypeCManualBase",
      "label": "Manual Test Left USB TypeC",
      "locals": {
        "usb": "eval! constants.typec_usb.left",
        "usb_label": "i18n! Left USB TypeC"
      }
    },
    "USBTypeCManualRight": {
      "inherit": "USBTypeCManualBase",
      "label": "Manual Test Right USB TypeC",
      "locals": {
        "usb": "eval! constants.typec_usb.right",
        "usb_label": "i18n! Right USB TypeC"
      }
    },
    "USBTypeCManualTest": {
      "inherit": "FlattenGroup",
      "subtests": [
        "USBTypeCManualLeft",
        "USBTypeCManualRight"
      ]
    },
    "USBTypeCTest": "USBTypeCManualTest",
    "UpdateCr50Firmware": {
      "inherit": "TestGroup",
      "label": "Update Cr50 Firmware",
      "subtests": [
        {
          "pytest_name": "update_cr50_firmware"
        },
        "RebootStep",
        {
          "pytest_name": "update_cr50_firmware",
          "label": "Check Cr50 Firmware Version",
          "args": {
            "method": "CHECK_VERSION"
          }
        }
      ]
    },
    "UpdateDeviceData": {
      "pytest_name": "update_device_data"
    },
    "UpdateFirmware": {
      "label": "Update Firmware",
      "subtests": [
        {
          "pytest_name": "update_firmware"
        },
        "Barrier",
        "RebootStep"
      ]
    },
    "UpdateSKUID": {
      "pytest_name": "update_sku"
    },
    "UploadRegCodes": {
      "inherit": "SyncFactoryServer",
      "label": "Upload Reg (ECHO) Codes to activate",
      "args": {
        "sync_event_logs": false,
        "sync_time": true,
        "update_toolkit": false,
        "upload_report": false,
        "upload_reg_codes": true,
        "flush_testlog": false
      }
    },
    "VerifyRootPartition": {
      "pytest_name": "verify_root_partition",
      "args": {
        "max_bytes": 1048576
      }
    },
    "WebGLAquarium": {
      "pytest_name": "webgl_aquarium"
    },
    "WifiSSIDList": {
      "pytest_name": "wifi_throughput",
      "label": "Wifi"
    },
    "Wireless": {
      "inherit": "WirelessRadiotap",
      "args": {
        "device_name": "wlan0",
        "services": [
          [
            "antenna_test",
            2412,
            null
          ]
        ],
        "strength": {
          "main": -60,
          "aux": -60,
          "all": -60
        }
      }
    },
    "WirelessAntenna": {
      "pytest_name": "wireless_antenna",
      "exclusive_resources": [
        "NETWORK"
      ]
    },
    "WirelessRadiotap": {
      "pytest_name": "wireless_radiotap",
      "exclusive_resources": [
        "NETWORK"
      ]
    },
    "WriteDeviceDataToVPD": {
      "pytest_name": "write_device_data_to_vpd"
    },
    "WriteHWID": {
      "pytest_name": "hwid",
      "label": "Write HWID",
      "args": {
        "enable_factory_server": "eval! constants.enable_factory_server",
        "run_vpd": "eval! constants.hwid_need_vpd",
        "rma_mode": "eval! constants.rma_mode"
      }
    },
    "WriteProtectSwitch": {
      "pytest_name": "write_protect_switch"
    }
  }
}

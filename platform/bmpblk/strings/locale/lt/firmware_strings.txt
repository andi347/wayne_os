„Chrome“ OS nėra arba ji pažeista.
Įdėkite atkūrimo USB atmintinę arba SD kortelę.
Įdėkite atkūrimo USB atmintinę.
Įdėkite atkūrimo SD kortelę arba USB atmintinę (pastaba: mėlynos spalvos USB prievadas NEVEIKS atkuriant).
Įdėkite atkūrimo USB atmintinę į vieną iš 4 prievadų įrenginio GALINĖJE pusėje.
Įdėtame įrenginyje nėra „Chrome“ OS.
OS tikrinimas IŠJUNGTAS
Paspauskite TARPĄ, kad iš naujo įgalintumėte.
Patvirtinkite, kad norite įjungti OS tikrinimą, paspaudę ENTER.
Sistema bus paleista iš naujo, o vietiniai duomenys išvalyti.
Jei norite grįžti, paspauskite ESC.
OS tikrinimas ĮJUNGTAS.
Jei norite IŠJUNGTI OS tikrinimą, paspauskite ENTER.
Pagalbos galite gauti apsilankę adresu https://google.com/chromeos/recovery
Klaidos kodas
Pašalinkite visus išorinius įrenginius, kad pradėtumėte atkūrimą.
Modelis 60061e
Jei norite IŠJUNGTI OS tikrinimą, paspauskite ATKŪRIMO mygtuką.
Prijungtas maitinimo šaltinis neteikia pakankamai energijos, kad šis įrenginys veiktų.
„Chrome“ OS dabar bus išjungta.
Naudokite tinkamą adapterį ir bandykite dar kartą.
Pašalinkite visus prijungtus įrenginius ir pradėkite atkūrimą.
Paspauskite skaitinį klavišą ir pasirinkite alternatyviąją paleidyklę:
Paspauskite MAITINIMO mygtuką, kad paleistumėte diagnostiką.
Jei norite IŠJUNGTI operacinės sistemos patvirtinimą, paspauskite MAITINIMO mygtuką.

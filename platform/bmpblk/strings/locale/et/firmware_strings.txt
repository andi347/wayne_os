Chrome OS puudub või on kahjustatud.
Sisestage taastamiseks mõeldud USB-mälupulk või SD-kaart.
Sisestage taastamiseks mõeldud USB-mälupulk.
Sisestage taastamiseks mõeldud SD-kaart või USB-mälupulk (märkus: sinine USB-port taastamise puhul EI tööta).
Sisestage taastamiseks mõeldud USB-mälupulk ühte neljast pordist seadme TAGAKÜLJEL.
Sisestatud seade ei sisalda Chrome OS-i.
OS-i kinnitamine on VÄLJAS
Uuesti lubamiseks vajutage TÜHIKUT.
OS-i kinnitamise sisselülitamise kinnitamiseks vajutage sisestusklahvi ENTER.
Teie süsteem lähtestatakse ja kohalikud andmed kustutatakse.
Tagasiminemiseks vajutage paoklahvi ESC.
OS-i kinnitamine on SEES.
OS-i kinnitamise VÄLJALÜLITAMISEKS vajutage sisestusklahvi ENTER.
Abi saamiseks külastage saiti https://google.com/chromeos/recovery
Veakood
Taastamise alustamiseks eemaldage kõik välised seadmed.
Mudel 60061e
OS-i kinnitamise VÄLJALÜLITAMISEKS vajutage nuppu TAASTAMINE.
Ühendatud toiteallikas pole seadme käitamiseks piisavalt võimas.
Chrome OS lülitub nüüd välja.
Kasutage õiget adapterit ja proovige uuesti.
Eemaldage kõik ühendatud seadmed ja alustage taastamist.
Alternatiivse käivituslaaduri valimiseks vajutage numbriklahvi:
Vajutage diagnostika käitamiseks TOITENUPPU.
Operatsioonisüsteemi kinnitamise VÄLJALÜLITAMISEKS vajutage TOITENUPPU.

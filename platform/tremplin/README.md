# Tremplin

Tremplin is the "springboard" daemon that runs in the [Termina] VM and
exposes a [gRPC] interface for managing [LXD] containers.

[gRPC]: https://grpc.io/
[LXD]: https://linuxcontainers.org/lxd/
[Termina]: https://chromium.googlesource.com/chromiumos/overlays/board-overlays/+/master/project-termina/

/*
 * Copyright 2018 The Chromium OS Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */
#ifndef _CONFIG_H_
#define _CONFIG_H_


/*
 * Take back credits for unknown connections (Set to prefer chip operation, clear to prefer connection correctness)
 */
#define HCI_TAKE_CREDS_FOR_UNKNOWN_CONNS    true

/*
 * How many items in the GATT client work queue
 */
#define GATT_CLI_WORK_Q_SIZE                64

/*
 * How many items fit in the GATT work Q
 */
#define GATT_WORK_Q_SIZE                    64

/*
 * How much can a GATT client buffer on us-as-server before we assume it is a DoS. Must be < 2^32
 */
#define GATT_SRV_PREPARE_WRITE_MAX_BYTES    1048576

/*
 * How many milliseconds from sending an indication as a server to deciding it timed out?
 */
#define ATT_IND_TIMEOUT                     30000

/*
 * How many milliseconds from sending a request as a client to deciding it timed out?
 */
#define ATT_REQ_TIMOUT                      30000

/*
 * How many items fit in the ATT work Q
 */
#define ATT_WORK_Q_SIZE                     128

/*
 * Where to store stack config/state
 */
extern const char pref_path[];

/*
 * AAPI will try to queue this much per socket before shutting off flow
 * Due to latencies in shutting off slow, the real amount queued could be
 * more.
 */
#define AAPI_RFC_MAX_BUF                    16384

/*
 * Android API layer will queue up only this many work items at a time
 */
#define AAPI_NUM_OUTSTANDING_WORK_ITEMS     256

/*
 * RFC layer will wait this many ms before transmitting for small packets
 */
#define RFC_TX_WAIT_MSEC                    50

/*
 * RFC layer will buffer this many bytes for connections without credit-based flow control
 */
#define RFC_MAX_CREDITLESS_QUEUE            1024

/*
 * RFC layer will wait this many ms before closing a control connection
 * with no data channels
 */
#define RFC_USE_TIMEOUT                     2000

/*
 * RFC layer will wait this many ms before closing a control connection
 * due to response to a command timeout. Yes, this value is obscenely
 * high, but the spec mandates it be on the order of tens of seconds
 */
#define RFC_CMD_TIMEOUT                     20000

/*
 * RFC layer will give the remote side this many credits (1..7)
 */
#define RFC_NUM_REMOTE_CREDITS              7

/*
 * RFC layer will send a credit packet if we already have thsi many
 * of the other side's credits. 1 .. RFC_NUM_REMOTE_CREDITS
 */
#define RFC_THRESH_HELD_CREDITS             5

/*
 * RFC layer will queue up only this many work items at a time
 */
#define RFC_NUM_OUTSTANDING_WORK_ITEMS      256

/*
 * Default LE connection parameters
 */
#define HCI_LE_CONN_INT_MIN                10 /* 12.5 ms */
#define HCI_LE_CONN_INT_MAX                20 /* 25.0 ms */
#define HCI_LE_CONN_LATENCY                10
#define HCI_LE_CONN_TIMEOUT                83  /* .83 s */
#define HCI_LE_CONN_USE_RAND_ADDR          false /* TODO: should be true but 00:00:00:00:00:00 will be used as the local random address if we don't set one */
#define HCI_LE_CONN_SCAN_INTERVAL          24 /* 15 ms */
#define HCI_LE_CONN_SCAN_WINDOW            6  /* 3.75 ms */

/*
 * Default LE scan parameters
 */
#define HCI_LE_SCAN_INTERVAL               24 /* 15 ms */
#define HCI_LE_SCAN_WINDOW                 4  /* 2.5 ms */

/*
 * How many bytes of RX data we're willing to buffer for a new ACL link during
 * configuration before we decide they're spamming us and drop the link.
 */
#define HCI_CONN_CFG_DATA_BACKLOG_MAX      65536

/*
 * How long we let other side wait for our decision till we auto-reject (in units of 0.625 ms)
 */
#define HCI_CONN_RX_TIMEOUT                16000

/*
 * How long our chip should keep trying to connect to a peer (in units of 0.625 ms)
 */
#define HCI_CONN_TX_TIMEOUT                7800

/*
 * HCI layer will queue up only this many cmds at a time
 */
#define HCI_NUM_OUTSTANDING_CMDS           64

/*
 * HCI layer will queue up only this many callbacks at a time
 */
#define HCI_NUM_OUTSTANDING_CBKS           256

/*
 * How many bytes of RX data we're willing to buffer for a new FixedCh conn during
 * service allocation.
 */
#define L2C_FIXED_CH_CONN_RX_BACKLOG_MAX   65536

/*
 * L2CAP layer will allow at most this many concurrent outstanding work requests
 * to the services running on top of it / under it. If you have a lot of
 * sometimes-slow services, you may want to set this high. All requests that
 * overflow this limit will be dropped.
 */
#define L2C_SVC_NUM_OUTSTANDING_REQS       512

/*
 * L2CAP layer will allow at most this many concurrent outstanding signalling frames
 */
#define L2C_SVC_NUM_OUTSTANDING_SIG_FRAMES 512

/*
 * How long to wait before closing an unused link (in case anyone else wants it)
 * value is in msec
 */
#define L2C_ACL_LINK_TIMOUT                5000

/*
 * How long to wait giving up on opening a link
 * value is in msec
 */
#define L2C_ACL_LINK_UP_TIMEOUT            16000

/*
 * How long to wait for the other side to acknowledge our request to open an L2CAP channel
 * value is in msec
 */
#define L2C_L2CAP_OPEN_TIMEOUT             2000

/*
 * How long to wait for the other side to finish authenticating us on open
 * value is in msec
 */
#define L2C_L2CAP_OPEN_PEND_TIMEOUT        70000

/*
 * How long L2CAP configuration state has to finish configuration
 * value is in msec
 */
#define L2C_L2CAP_CONFIG_TIMEOUT           2000

/*
 * How many seconds till we decide the chip is dead
 */
#define BT_COMMAND_TIMEOUT                 10

/* Max number of packets for the Security Manager sendQueue */
#define MAX_PACKETS_SM_SEND_QUEUE           8

/*
 * How many notifications to the upper layer that SM is willing to buffer up
 */
#define SM_NUM_OUTSTANDING_NOTIFY          32

#endif


From d54a3c2fec0af1e3273612b28732320c52fbe36c Mon Sep 17 00:00:00 2001
From: Joel Hockey <joelhockey@chromium.org>
Date: Sun, 3 Feb 2019 14:03:50 -0800
Subject: [PATCH 6/9] storage: Add ioprogress.ProgressTracker field to storage

This is part 4 of a series of patches to add better progress
tracking support for export and import.

Add a tracker field to ContainerCreateFromImage and ImageCreate
which can be used during Unpack.

Signed-off-by: Joel Hockey <joelhockey@chromium.org>
---
 lxd/container.go         | 5 +++--
 lxd/containers_post.go   | 4 ++--
 lxd/images.go            | 2 +-
 lxd/storage.go           | 4 ++--
 lxd/storage_btrfs.go     | 9 +++++----
 lxd/storage_ceph.go      | 7 ++++---
 lxd/storage_dir.go       | 5 +++--
 lxd/storage_lvm.go       | 5 +++--
 lxd/storage_lvm_utils.go | 2 +-
 lxd/storage_mock.go      | 5 +++--
 lxd/storage_zfs.go       | 7 ++++---
 11 files changed, 31 insertions(+), 24 deletions(-)

diff --git a/lxd/container.go b/lxd/container.go
index 1c9d5655..1685ec16 100644
--- a/lxd/container.go
+++ b/lxd/container.go
@@ -20,6 +20,7 @@ import (
 	"github.com/lxc/lxd/shared"
 	"github.com/lxc/lxd/shared/api"
 	"github.com/lxc/lxd/shared/idmap"
+	"github.com/lxc/lxd/shared/ioprogress"
 	"github.com/lxc/lxd/shared/logger"
 	"github.com/lxc/lxd/shared/osarch"
 )
@@ -631,7 +632,7 @@ func containerCreateEmptySnapshot(s *state.State, args db.ContainerArgs) (contai
 	return c, nil
 }
 
-func containerCreateFromImage(d *Daemon, args db.ContainerArgs, hash string) (container, error) {
+func containerCreateFromImage(d *Daemon, args db.ContainerArgs, hash string, tracker *ioprogress.ProgressTracker) (container, error) {
 	s := d.State()
 
 	// Get the image properties
@@ -686,7 +687,7 @@ func containerCreateFromImage(d *Daemon, args db.ContainerArgs, hash string) (co
 	}
 
 	// Now create the storage from an image
-	err = c.Storage().ContainerCreateFromImage(c, hash)
+	err = c.Storage().ContainerCreateFromImage(c, hash, tracker)
 	if err != nil {
 		c.Delete()
 		return nil, err
diff --git a/lxd/containers_post.go b/lxd/containers_post.go
index d581498e..3c8e95fa 100644
--- a/lxd/containers_post.go
+++ b/lxd/containers_post.go
@@ -121,7 +121,7 @@ func createFromImage(d *Daemon, req *api.ContainersPost) Response {
 			return err
 		}
 
-		_, err = containerCreateFromImage(d, args, info.Fingerprint)
+		_, err = containerCreateFromImage(d, args, info.Fingerprint, nil)
 		return err
 	}
 
@@ -338,7 +338,7 @@ func createFromMigration(d *Daemon, req *api.ContainersPost) Response {
 		}
 
 		if ps.MigrationType() == migration.MigrationFSType_RSYNC {
-			c, err = containerCreateFromImage(d, args, req.Source.BaseImage)
+			c, err = containerCreateFromImage(d, args, req.Source.BaseImage, nil)
 			if err != nil {
 				return InternalError(err)
 			}
diff --git a/lxd/images.go b/lxd/images.go
index 60647725..88715c6f 100644
--- a/lxd/images.go
+++ b/lxd/images.go
@@ -607,7 +607,7 @@ func imageCreateInPool(d *Daemon, info *api.Image, storagePool string) error {
 
 	// Create the storage volume for the image on the requested storage
 	// pool.
-	err = s.ImageCreate(info.Fingerprint)
+	err = s.ImageCreate(info.Fingerprint, nil)
 	if err != nil {
 		return err
 	}
diff --git a/lxd/storage.go b/lxd/storage.go
index 15a1e295..2c07b332 100644
--- a/lxd/storage.go
+++ b/lxd/storage.go
@@ -169,7 +169,7 @@ type storage interface {
 	ContainerCreate(container container) error
 
 	// ContainerCreateFromImage creates a container from a image.
-	ContainerCreateFromImage(c container, fingerprint string) error
+	ContainerCreateFromImage(c container, fingerprint string, tracker *ioprogress.ProgressTracker) error
 	ContainerCanRestore(target container, source container) error
 	ContainerDelete(c container) error
 	ContainerCopy(target container, source container, containerOnly bool) error
@@ -191,7 +191,7 @@ type storage interface {
 	ContainerSnapshotCreateEmpty(c container) error
 
 	// Functions dealing with image storage volumes.
-	ImageCreate(fingerprint string) error
+	ImageCreate(fingerprint string, tracker *ioprogress.ProgressTracker) error
 	ImageDelete(fingerprint string) error
 	ImageMount(fingerprint string) (bool, error)
 	ImageUmount(fingerprint string) (bool, error)
diff --git a/lxd/storage_btrfs.go b/lxd/storage_btrfs.go
index 853722d3..497fa06c 100644
--- a/lxd/storage_btrfs.go
+++ b/lxd/storage_btrfs.go
@@ -22,6 +22,7 @@ import (
 	"github.com/lxc/lxd/shared"
 	"github.com/lxc/lxd/shared/api"
 	"github.com/lxc/lxd/shared/idmap"
+	"github.com/lxc/lxd/shared/ioprogress"
 	"github.com/lxc/lxd/shared/logger"
 )
 
@@ -762,7 +763,7 @@ func (s *storageBtrfs) ContainerCreate(container container) error {
 }
 
 // And this function is why I started hating on btrfs...
-func (s *storageBtrfs) ContainerCreateFromImage(container container, fingerprint string) error {
+func (s *storageBtrfs) ContainerCreateFromImage(container container, fingerprint string, tracker *ioprogress.ProgressTracker) error {
 	logger.Debugf("Creating BTRFS storage volume for container \"%s\" on storage pool \"%s\"", s.volume.Name, s.pool.Name)
 
 	source := s.pool.Config["source"]
@@ -805,7 +806,7 @@ func (s *storageBtrfs) ContainerCreateFromImage(container container, fingerprint
 
 		var imgerr error
 		if !shared.PathExists(imageMntPoint) || !isBtrfsSubVolume(imageMntPoint) {
-			imgerr = s.ImageCreate(fingerprint)
+			imgerr = s.ImageCreate(fingerprint, tracker)
 		}
 
 		lxdStorageMapLock.Lock()
@@ -1417,7 +1418,7 @@ func (s *storageBtrfs) ContainerSnapshotCreateEmpty(snapshotContainer container)
 	return nil
 }
 
-func (s *storageBtrfs) ImageCreate(fingerprint string) error {
+func (s *storageBtrfs) ImageCreate(fingerprint string, tracker *ioprogress.ProgressTracker) error {
 	logger.Debugf("Creating BTRFS storage volume for image \"%s\" on storage pool \"%s\"", fingerprint, s.pool.Name)
 
 	// Create the subvolume.
@@ -1469,7 +1470,7 @@ func (s *storageBtrfs) ImageCreate(fingerprint string) error {
 
 	// Unpack the image in imageMntPoint.
 	imagePath := shared.VarPath("images", fingerprint)
-	err = unpackImage(imagePath, tmpImageSubvolumeName, storageTypeBtrfs, s.s.OS.RunningInUserNS, nil)
+	err = unpackImage(imagePath, tmpImageSubvolumeName, storageTypeBtrfs, s.s.OS.RunningInUserNS, tracker)
 	if err != nil {
 		return err
 	}
diff --git a/lxd/storage_ceph.go b/lxd/storage_ceph.go
index 255c5b09..f2936eb8 100644
--- a/lxd/storage_ceph.go
+++ b/lxd/storage_ceph.go
@@ -12,6 +12,7 @@ import (
 	"github.com/lxc/lxd/lxd/state"
 	"github.com/lxc/lxd/shared"
 	"github.com/lxc/lxd/shared/api"
+	"github.com/lxc/lxd/shared/ioprogress"
 	"github.com/lxc/lxd/shared/logger"
 
 	"github.com/pborman/uuid"
@@ -749,7 +750,7 @@ func (s *storageCeph) ContainerCreate(container container) error {
 	return nil
 }
 
-func (s *storageCeph) ContainerCreateFromImage(container container, fingerprint string) error {
+func (s *storageCeph) ContainerCreateFromImage(container container, fingerprint string, tracker *ioprogress.ProgressTracker) error {
 	logger.Debugf(`Creating RBD storage volume for container "%s" on storage pool "%s"`, s.volume.Name, s.pool.Name)
 
 	revert := true
@@ -790,7 +791,7 @@ func (s *storageCeph) ContainerCreateFromImage(container container, fingerprint
 		}
 
 		if !ok {
-			imgerr = s.ImageCreate(fingerprint)
+			imgerr = s.ImageCreate(fingerprint, tracker)
 		}
 
 		lxdStorageMapLock.Lock()
@@ -1793,7 +1794,7 @@ func (s *storageCeph) ContainerSnapshotCreateEmpty(c container) error {
 	return nil
 }
 
-func (s *storageCeph) ImageCreate(fingerprint string) error {
+func (s *storageCeph) ImageCreate(fingerprint string, tracker *ioprogress.ProgressTracker) error {
 	logger.Debugf(`Creating RBD storage volume for image "%s" on storage pool "%s"`, fingerprint, s.pool.Name)
 
 	revert := true
diff --git a/lxd/storage_dir.go b/lxd/storage_dir.go
index f2814e6d..62b24e8d 100644
--- a/lxd/storage_dir.go
+++ b/lxd/storage_dir.go
@@ -14,6 +14,7 @@ import (
 	"github.com/lxc/lxd/shared"
 	"github.com/lxc/lxd/shared/api"
 	"github.com/lxc/lxd/shared/idmap"
+	"github.com/lxc/lxd/shared/ioprogress"
 	"github.com/lxc/lxd/shared/logger"
 )
 
@@ -479,7 +480,7 @@ func (s *storageDir) ContainerCreate(container container) error {
 	return nil
 }
 
-func (s *storageDir) ContainerCreateFromImage(container container, imageFingerprint string) error {
+func (s *storageDir) ContainerCreateFromImage(container container, imageFingerprint string, tracker *ioprogress.ProgressTracker) error {
 	logger.Debugf("Creating DIR storage volume for container \"%s\" on storage pool \"%s\"", s.volume.Name, s.pool.Name)
 
 	_, err := s.StoragePoolMount()
@@ -1008,7 +1009,7 @@ func (s *storageDir) ContainerSnapshotStop(container container) (bool, error) {
 	return true, nil
 }
 
-func (s *storageDir) ImageCreate(fingerprint string) error {
+func (s *storageDir) ImageCreate(fingerprint string, tracker *ioprogress.ProgressTracker) error {
 	return nil
 }
 
diff --git a/lxd/storage_lvm.go b/lxd/storage_lvm.go
index 944bf947..5425cd23 100644
--- a/lxd/storage_lvm.go
+++ b/lxd/storage_lvm.go
@@ -15,6 +15,7 @@ import (
 	"github.com/lxc/lxd/shared"
 	"github.com/lxc/lxd/shared/api"
 	"github.com/lxc/lxd/shared/idmap"
+	"github.com/lxc/lxd/shared/ioprogress"
 	"github.com/lxc/lxd/shared/logger"
 )
 
@@ -928,7 +929,7 @@ func (s *storageLvm) ContainerCreate(container container) error {
 	return nil
 }
 
-func (s *storageLvm) ContainerCreateFromImage(container container, fingerprint string) error {
+func (s *storageLvm) ContainerCreateFromImage(container container, fingerprint string, tracker *ioprogress.ProgressTracker) error {
 	logger.Debugf("Creating LVM storage volume for container \"%s\" on storage pool \"%s\"", s.volume.Name, s.pool.Name)
 
 	tryUndo := true
@@ -1561,7 +1562,7 @@ func (s *storageLvm) ContainerSnapshotCreateEmpty(snapshotContainer container) e
 	return nil
 }
 
-func (s *storageLvm) ImageCreate(fingerprint string) error {
+func (s *storageLvm) ImageCreate(fingerprint string, tracker *ioprogress.ProgressTracker) error {
 	logger.Debugf("Creating LVM storage volume for image \"%s\" on storage pool \"%s\"", fingerprint, s.pool.Name)
 
 	tryUndo := true
diff --git a/lxd/storage_lvm_utils.go b/lxd/storage_lvm_utils.go
index 6370fd3c..794f40ac 100644
--- a/lxd/storage_lvm_utils.go
+++ b/lxd/storage_lvm_utils.go
@@ -504,7 +504,7 @@ func (s *storageLvm) containerCreateFromImageThinLv(c container, fp string) erro
 		}
 
 		if !ok {
-			imgerr = s.ImageCreate(fp)
+			imgerr = s.ImageCreate(fp, nil)
 		}
 
 		lxdStorageMapLock.Lock()
diff --git a/lxd/storage_mock.go b/lxd/storage_mock.go
index 8064a6d3..c0b36f78 100644
--- a/lxd/storage_mock.go
+++ b/lxd/storage_mock.go
@@ -9,6 +9,7 @@ import (
 	"github.com/lxc/lxd/lxd/state"
 	"github.com/lxc/lxd/shared/api"
 	"github.com/lxc/lxd/shared/idmap"
+	"github.com/lxc/lxd/shared/ioprogress"
 	"github.com/lxc/lxd/shared/logger"
 )
 
@@ -118,7 +119,7 @@ func (s *storageMock) ContainerCreate(container container) error {
 }
 
 func (s *storageMock) ContainerCreateFromImage(
-	container container, imageFingerprint string) error {
+	container container, imageFingerprint string, tracker *ioprogress.ProgressTracker) error {
 
 	return nil
 }
@@ -189,7 +190,7 @@ func (s *storageMock) ContainerSnapshotCreateEmpty(snapshotContainer container)
 	return nil
 }
 
-func (s *storageMock) ImageCreate(fingerprint string) error {
+func (s *storageMock) ImageCreate(fingerprint string, tracker *ioprogress.ProgressTracker) error {
 	return nil
 }
 
diff --git a/lxd/storage_zfs.go b/lxd/storage_zfs.go
index f49c1fcc..eb93168e 100644
--- a/lxd/storage_zfs.go
+++ b/lxd/storage_zfs.go
@@ -19,6 +19,7 @@ import (
 	"github.com/lxc/lxd/shared"
 	"github.com/lxc/lxd/shared/api"
 	"github.com/lxc/lxd/shared/idmap"
+	"github.com/lxc/lxd/shared/ioprogress"
 	"github.com/lxc/lxd/shared/logger"
 
 	"github.com/pborman/uuid"
@@ -769,7 +770,7 @@ func (s *storageZfs) ContainerCreate(container container) error {
 	return nil
 }
 
-func (s *storageZfs) ContainerCreateFromImage(container container, fingerprint string) error {
+func (s *storageZfs) ContainerCreateFromImage(container container, fingerprint string, tracker *ioprogress.ProgressTracker) error {
 	logger.Debugf("Creating ZFS storage volume for container \"%s\" on storage pool \"%s\"", s.volume.Name, s.pool.Name)
 
 	containerPath := container.Path()
@@ -793,7 +794,7 @@ func (s *storageZfs) ContainerCreateFromImage(container container, fingerprint s
 
 		var imgerr error
 		if !zfsFilesystemEntityExists(poolName, fsImage) {
-			imgerr = s.ImageCreate(fingerprint)
+			imgerr = s.ImageCreate(fingerprint, tracker)
 		}
 
 		lxdStorageMapLock.Lock()
@@ -1696,7 +1697,7 @@ func (s *storageZfs) ContainerSnapshotCreateEmpty(snapshotContainer container) e
 // - mark new zfs volume images/<fingerprint> readonly
 // - remove mountpoint property from zfs volume images/<fingerprint>
 // - create read-write snapshot from zfs volume images/<fingerprint>
-func (s *storageZfs) ImageCreate(fingerprint string) error {
+func (s *storageZfs) ImageCreate(fingerprint string, tracker *ioprogress.ProgressTracker) error {
 	logger.Debugf("Creating ZFS storage volume for image \"%s\" on storage pool \"%s\"", fingerprint, s.pool.Name)
 
 	poolName := s.getOnDiskPoolName()
-- 
2.20.1.791.gb4d0f1c61a-goog


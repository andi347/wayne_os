#!/bin/bash
#
# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# Script for pumping data for the dashboard, usually called by cron.
# On first run, pass --init-days=60 to initialize the database

SRCDIR=/whining
export PYTHONPATH=$SRCDIR:$PYTHONPATH

# Copy data to rawdb (from lab DB, builders and suite_schedule.ini).
python $SRCDIR/src/backend/update_rawdb.py "$@"

# Copying data from rawdb to local back db
python $SRCDIR/src/backend/update_wmdb.py

%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%def body_block():
  %# --------------------------------------------------------------------------
  %# Switcher toolbar allows query parameter removal.
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base='suitestats')

  %#-------------------------------------------------------------------------
  %# A table with suite details.
  %#-------------------------------------------------------------------------
  %_top_title = 'Suite Statistics'
  %_cell_classes = {'_default': 'centered'}
  %_head_classes = {'_default': 'headeritem centered'}
  %_table_data = tpl_vars['data']['table_data']
  %_ignore_cols = ['row_key']
  {{! _table_data.simple_table(_top_title, _cell_classes, _head_classes, [], _ignore_cols) }}
%end

%rebase('master.tpl', title='suitestats', query_string=query_string, body_block=body_block)

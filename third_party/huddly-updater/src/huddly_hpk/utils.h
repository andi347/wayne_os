// Copyright 2019 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef SRC_HUDDLY_HPK_UTILS_H_
#define SRC_HUDDLY_HPK_UTILS_H_

#include <cstdint>
#include <string>

namespace huddly {

std::string Uint8ToHexString(uint8_t value);
std::string Uint16ToHexString(uint16_t value);
std::string UsbVidPidString(uint16_t vid, uint16_t pid);

}  // namespace huddly

#endif  //  SRC_HUDDLY_HPK_UTILS_H_
